#!/usr/bin/env bash

cat <<EOF

############################
#     Send to Stake        #
############################

EOF

function stake() {
  operator-cli stake 10.0
}

echo "Your wallet address: $ETHEREUM_ADDRESS, and Private key: ${PRIV_KEY}"
operator-cli status | grep standby
if (( $? != 0 )) 
then
  stake
else
  operator-cli start
  sleep 10
  stake
fi
